eventsApp.factory('eventData', function($resource){
    var resource = $resource('/data/event/:id', {id:'@id'});
    
    return {
        getEvent: function(eventId) {
            return resource.get({id:eventId});
        },
        save: function(event){
            //Replace with an actual id generator
            event.id = 999;
            return resource.save(event);
        },
        getAllEvents: function(){
            return resource.query();
        }
    };
});