# Angular Event Registration — Demo Application for Angular JS

Angular Version 1.4.0

This project is a sample application demonstrating a more fully developed realistic single page
angular application.

### Running the application

Run `npm install` and then `npm start`

### Running the unit tests

Run `npm test`

### Running the end-to-end tests

Install protractor (Selenium based testing suite wrapping web-driver)

Run `npm run end-to-end-test` and then in another terminal run `npm run protractor`



